# Use the base image with Alpine Linux
FROM openjdk:17-alpine

# Set the working directory in the container
WORKDIR /app

# Copy the Maven wrapper script
COPY mvnw .
COPY .mvn .mvn

# Copy the Maven project descriptor files
COPY pom.xml .

# Download dependencies and build the project
RUN ./mvnw dependency:go-offline

# Copy the project source code
COPY src src

# Package the application
RUN ./mvnw package -DskipTests

# Set the entry point for the Docker container
ENTRYPOINT ["java", "-jar", "target/petclinic.jar"]
